import Image from 'next/image';
import React from 'react';

const SideCard = () => {
    return (
        <div className='sideCardContainer'>
            <div className='sideCardContainerHeader'>The Future</div>
            <Image
                src="/images/gamingLogo.jpeg"
                width={100}
                height={100}
                alt="logo" />
            <div className='sideCardContainerFooter'>Free your mind for something more important</div>
        </div>
    );
};

export default SideCard;