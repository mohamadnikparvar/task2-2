import Image from 'next/image';
import React from 'react';
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';
import ApiIcon from '@mui/icons-material/Api';

const MainCard = () => {
    return (
        <div className='mainCardContainer'>
            <ApiIcon style={{ margin: '10px' }} />
            <div className='mainCardImage'>
                <Image
                    src="/images/gamingLogo.jpeg"
                    width={200}
                    height={200}
                    alt="logo" />
                <div className='mainCardFooter'>
                    <div className='mainCardFooterText'>because your business needs a lot more</div>
                    <div className='mainCardFooterButton'>
                        <p>get expert advice</p>
                        <ArrowCircleRightIcon />
                    </div>

                </div>
            </div>

        </div>
    );
};

export default MainCard;